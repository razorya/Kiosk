import requests
from urllib2 import urlopen as uReq
from bs4 import BeautifulSoup as soup
from openpyxl import Workbook,load_workbook
import os


wb = Workbook()
os.chdir('D:\\ResearchExcel')
wb = load_workbook('stock.xlsx')
ws = wb.worksheets[0]
print 'ws',ws

names = wb.sheetnames
sheet1 = str(names[0])
print sheet1
#sheet1.title = 'Stock name'

#sheet1['A1'] = "BDMS"
#sheet1.cell(row=2, column=2).value = 2

url = 'http://www.settrade.com/C04_01_stock_quote_p1.jsp?txtSymbol=BDMS&ssoPageId=9&selectPage='
uClient = uReq(url)
page_html = uClient.read()
uClient.close()

page_soup = soup(page_html,"html.parser")
price = page_soup.findAll("div",{"class":"col-xs-6 col-xs-offset-6 colorGreen"})
price = page_soup.findAll("div",{"class":"col-xs-6 col-xs-offset-6 colorRed"})
last_trade = price[0].text.strip()

date = page_soup.findAll("span" ,{"class":"colorGreen"})
last_update = date[0].text.strip()

percentage_change = page_soup.findAll("h1",{"class":"colorGreen"})
percentage_change = page_soup.findAll("h1",{"class":"colorRed"})
last_percentage_change = percentage_change[0].text.strip()
last_change = percentage_change[1].text.strip()

table_td = page_soup.findAll("td")
prior = table_td[1].text.strip()
open = table_td[3].text.strip()
high = table_td[5].text.strip()
low = table_td[7].text.strip()
average_price = table_td[9].text.strip()


print "Stock BDMS"
print "Last trade is %s Baht" % last_trade
print "Last update is %s" % last_update
print "Change is %s" % last_percentage_change
print "Percentage change is %s" % last_change
print '____________________'
print "Prior is %s" % prior
print "Open is %s" % open
print "High is %s" % high
print "Low is %s" % low
print "Average price is %s" % average_price