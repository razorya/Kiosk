from openpyxl import Workbook,load_workbook
import os
from urllib2 import urlopen as uReq
from bs4 import BeautifulSoup as soup

os.chdir('D:\\ResearchExcel')
wb = load_workbook('stock.xlsx')
names = wb.sheetnames
sheet1 = str(names[0])
sheet2 = str(names[1])
ws1 = wb[sheet1]
ws2 = wb[sheet2]


url = 'http://www.settrade.com/C04_01_stock_quote_p1.jsp?txtSymbol=BDMS&ssoPageId=9&selectPage='
uClient = uReq(url)
page_html = uClient.read()
uClient.close()
page_soup = soup(page_html,"html.parser")

# displayed xml page
#print (page_soup.prettify())
#match = page_soup.find("ul", class_='subnav-lv3')
#match2 = match.li.a.text
#match3 = match.text
#match4 = page_soup.find("ul", class_='subnav-lv3').li.text
#print match
#print '_________________'
#print match2
#print match3
#print match4


price = page_soup.findAll("div",{"class":"col-xs-6 col-xs-offset-6 colorGreen"})
price = page_soup.findAll("div",{"class":"col-xs-6 col-xs-offset-6 colorRed"})
if price == []:
    price = page_soup.findAll("h1")
    last_trade = price[1].text.strip()
    ws1['B4'] = last_trade
else:
    last_trade = price[0].text.strip()
    ws1['B4'] = last_trade


date = page_soup.findAll("span" ,{"class":"colorGreen"})
last_update = date[0].text.strip()
ws1['B1'] = last_update


change = page_soup.findAll("h1",{"class":"colorGreen"})
change = page_soup.findAll("h1",{"class":"colorRed"})
if change == []:
    percentage_change = page_soup.findAll("h1", {"class": ""})
    try:
        last_percentage_change = percentage_change[2].text.strip()
        last_change = percentage_change[3].text.strip()
        ws1['A6'] = last_percentage_change
        ws1['B6'] = last_change
    except IndexError:
        print 'list index out of range because price not change'
else:
    last_percentage_change = change[0].text.strip()
    last_change = change[1].text.strip()
    ws1['A6'] = last_percentage_change
    ws1['B6'] = last_change


table_td = page_soup.findAll("td")
prior = table_td[1].text.strip()
open = table_td[3].text.strip()
high = table_td[5].text.strip()
low = table_td[7].text.strip()
average_price = table_td[9].text.strip()
ws1['E2'] = prior
ws1['E3'] = open
ws1['E4'] = high
ws1['E5'] = low
ws1['E6'] = average_price


print "sheet1 name is",sheet1
print "sheet2 name is",sheet2
ws1['A3'] = 'BDMS'

wb.save('stock.xlsx')

